package deloitte.ccademy.lesson01.arreglos;

public class Entero {

	/*
	 * public static void main(String[] args) {
	 * 
	 * 
	 * 
	 * 
	 * 
	 * {int [] marks = {125,132,95,116,110,110,110};
	 * 
	 * 
	 * int highest_marks = Maximum(marks); int lowest_marks = Menor(marks);
	 * LOGGER.info("Esta es la suma: " + highest_marks);
	 * 
	 * System.out.println("The lowest score is " + lowest_marks); }}
	 * 
	 */

	public int Maximum(int[] numbers) {
		int maxSoFar = numbers[0];

		for (int num : numbers) {
			if (num > maxSoFar) {
				maxSoFar = num;
			}

		}
		return maxSoFar;
	}

	public static int Menor(int[] numbers) {
		int maxSoFarMenor = numbers[0];

		for (int num : numbers) {
			if (num < maxSoFarMenor) {
				maxSoFarMenor = num;
			}

		}
		return maxSoFarMenor;
	}
}
