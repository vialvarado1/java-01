
/*
 *  @Autor Victor alvarado
 */
package deloitte.Academy.lesson01.arithmetic;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Arithmetic {

	/*
	 * Este programa hace todas las operaciones aritmeticas
	 */
	private static final Logger LOGGER = Logger.getLogger(Arithmetic.class.getName());

	/*
	 * public int adittion es la generacion de nuestro metodo
	 */
	
	/**
	 * 
	 * @param valor1 Valor del tipo entero para la primera Suma
	 * @param valor2 Valor del tipo entero para la primera Suma
	 * @return regresa un entero
	 */
	public int addition(int valor1, int valor2) {
		int result = 0;

		/*
		 * Se declaran las variables incluyendo el resultado
		 */
		try {

			/*
			 * Se inicia una suma
			 */
			result = valor1 + valor2;
			LOGGER.info("Esta es la suma: " + result);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;

	}

	
	/**
	 * 
	 * @param valor1 Valor del tipo entero para la primera División
	 * @param valor2 Valor del tipo entero para la primera División
	 * @return regresa un entero
	 */
	
	public int division(int valor1, int valor2) {
		int result = 0;
		/*
		 * Se inicia una division
		 */

		try {
			result = valor1 / valor2;
			LOGGER.info("Esta es la división " + result);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;

	}
	
	
	/**
	 * 
	 * @param valor1 Valor del tipo entero para la primera resta
	 * @param valor2 Valor del tipo entero para la primera resta
	 * @return va a regresar un entero
	 */

	public int resta(int valor1, int valor2) {
		int result = 0;
		try {
			/*
			 * Se inicia una resta
			 */

			result = valor1 - valor2;
			LOGGER.info("Esta es la resta " + result);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;
	}

	
	/**
	 * 
	 * @param valor1 Valor del tipo entero para la primera multiplicación
	 * @param valor2 Valor del tipo entero para la primera multiplicación
	 * @return Va a regresar un entero
	 */
	public int multiplication(int valor1, int valor2) {
		int result = 0;
		try {
			/*
			 * Se inicia una multiplicacion
			 */
			
			result = valor1 * valor2;
			LOGGER.info("Esta es la multiplicacion " + result);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;
	}

	
	
	/**
	 * 
	 * @param valor1 Valor del tipo entero para el primer modulo
	 * @param valor2 Valor del tipo entero para el primer modulo
	 * @return va a regresar un entero
	 */
	
	public int modulo(int valor1, int valor2) {
		int result = 0;
		try {
			/*
			 * Se inicia un modulo
			 */
			result = valor1 % valor2;
			LOGGER.info("Esta es el modulo " + result);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;
		/*
		 * Se imprimen resultados
		 */
	}
}
