/*
 *  @Autor Victor alvarado
 */
package deloitte.Academy.lesson01;

import deloitte.Academy.lesson01.arithmetic.Arithmetic;
import deloitte.Academy.lesson01.logicaloperators.LogicalOperators;
import deloitte.Academy.lesson01.comparations.Comparations;
/*
 *   Importan los metodos
 */

public class run {
	/*
	 * Se mandan a traer los metodos
	 */
	
	/**
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Arithmetic arithmetic = new Arithmetic();
		LogicalOperators logicaloperators = new LogicalOperators();
		Comparations comparations = new Comparations();
		int mensaje = arithmetic.addition(12, 14);
		System.out.println("La suma es: " + mensaje);
		/*
		 * Se imprimen los resultados
		 */
		int mensaje2 = arithmetic.division(30, 60);
		System.out.println("La división es: " + mensaje2);
		/*
		 * Se imprimen los resultados
		 */

		int mensaje3 = arithmetic.resta(10, 50);
		System.out.println("La resta es: " + mensaje3);
		/*
		 * Se imprimen los resultados
		 */

		int mensaje4 = arithmetic.multiplication(10, 50);
		System.out.println("La multiplicacion es: " + mensaje4);
		/*
		 * Se imprimen los resultados
		 */

		int mensaje5 = arithmetic.modulo(10, 50);
		System.out.println("El modulo es: " + mensaje5);

		/*
		 * Se imprimen los resultados
		 */

		String mensaje6 = logicaloperators.Condicional(10, 50);
		System.out.println("Fin de la comparación 1 " + mensaje6);

		/*
		 * Se imprimen los resultados
		 */

		String mensaje7 = logicaloperators.Condicional2(15, 50);
		System.out.println("Fin de la comparacion 2 " + mensaje7);

		/*
		 * Se imprimen los resultados
		 */

		String mensaje8 = logicaloperators.Condicional3(10, 10);
		System.out.println("Fin de la comparacion 3 " + mensaje8);

		/*
		 * Se imprimen los resultados
		 */

		String mensaje9 = comparations.Comparativa1(10, 10);
		System.out.println("Resultado de la Comparativa:  " + mensaje9);
		/*
		 * Se imprimen los resultados
		 */

	}

}
