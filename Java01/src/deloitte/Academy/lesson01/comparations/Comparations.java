
/*
 * @ Autor Victor alvarado
 */

package deloitte.Academy.lesson01.comparations;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Comparations {

	/*
	 * Este programa haca una comparativa entre dos numeros
	 */
	private static final Logger LOGGER = Logger.getLogger(Comparations.class.getName());

	/*
	 * Comparativa 1 El valor 1 es mayor que el valor 2, los valores son iguales, si
	 * no el valor 2 es mayor que el valor 1
	 */
	/**
	 * 
	 * @param valor1 Valor de tipo entero para comparación del primer valor
	 * @param valor2 Valor de tipo entero para comparación del segundo valor
	 * @return Va a regresar un entero
	 */
	public String Comparativa1(int valor1, int valor2) {
		String result = "";

		/*
		 * Compara el valor 1 con el valor 2
		 */
		try {
			if (valor1 < valor2) {
				result = "Valor 1 es mayor que valor 2";

				LOGGER.info("Valor 1: " + valor1 + "valor 2:" + valor2);
			}

			/*
			 * Si valor 1 es igual que el valor 2 Imprimir
			 */
			if (valor1 == valor2) {
				result = "Los valores son iguales";

				LOGGER.info("Valor 1: " + valor1 + "es igual que valor 2:" + valor2);
			} else {
				System.out.println("Valor 2 Es mayor que valor 1");
				LOGGER.info("Valor 2 Es mayor que valor 1");

				/*
				 * Si no valor 2 es mayor que valor 1
				 */

			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;

		/*
		 * Retorna el resultado
		 */

	}

	/**
	 * 
	 * @param valor1 Variable del tipo entero para la comparativa 2
	 * @param valor2 Variable del tipo entero para la compararitiva
	 * @return va a regresar un entero
	 */
	public String Comparativa2(int valor1, int valor2) {
		String result = "";

		try {
			if (valor1 == valor2) {
				result = "Ambos valores son iguales";

				LOGGER.info("Valor 1:   " + valor1 + "igual que:   " + valor2);
			} else {
				System.out.println("No son Iguales");
				LOGGER.info("Valor 1: " + valor1 + "=/=" + valor2);
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;

	}

}
