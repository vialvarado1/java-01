/*
 * @ autor: Victor Alvarado
 */

package deloitte.Academy.lesson01.logicaloperators;

import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Este programa hace una comparaci�n logica entre dos literales, y dos constantes
 */

public class LogicalOperators {

	/*
	 * Primer condicional logica, los valores tienen que ser iguales
	 */

	private static final Logger LOGGER = Logger.getLogger(LogicalOperators.class.getName());

	/**
	 * 
	 * @param valor1 Valor del tipo entero para el primer condicional
	 * @param valor2 Valor del tipo entero para el primer condicional
	 * @return
	 */
	public String Condicional(int valor1, int valor2) {
		String result = "";
		/*
		 * Public String Condicional es nuestro m�todo Se declaran dos valores: valor 1
		 * y valor 2 del tipo entero
		 * 
		 */
		try {
			if (valor1 == 10 && valor2 == 23) {
				result = "Se cumpli� condici�n";

				LOGGER.info("Se cumplen ambas condiciones");

				/*
				 * If hace una comparaci�n logica entre ambos valores,
				 */

			} else {
				System.out.println("No se cumple la condici�n");
				LOGGER.info("No se cumplen ambas condiciones");

			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;

		/*
		 * return result nos permite obtener el resultado de las condiciones
		 */

	}

	/*
	 * Segunda condicional logica, los valores tienen que ser valor 1 mayor igual a
	 * 10, valor 2 menor igual a 23
	 */

	/**
	 * 
	 * @param valor1 Valor del tipo entero para el primer condicional
	 * @param valor2 Valor del tipo entero para el primer condicional
	 * @return
	 */

	public String Condicional2(int valor1, int valor2) {
		String result = "";

		try {
			if (valor1 >= 10 && valor2 <= 23) {
				result = "Se cumpli� condici�n";

				LOGGER.info("Se cumplen ambas condiciones");
			} else {
				System.out.println("No se cumple la condici�n");
				LOGGER.info("No se cumplen ambas condiciones");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;

	}

	/*
	 * Tercer condicional logica, el valor 1 y valor 2 iguales a 10
	 */

	/**
	 * 
	 * @param valor1 Valor del tipo entero para el primer condicional
	 * @param valor2 Valor del tipo entero para el primer condicional
	 * @return
	 */

	public String Condicional3(int valor1, int valor2) {
		String result = "";

		try {
			if (valor1 == 10 && valor2 == 10) {
				result = "Se cumpli� condici�n";

				LOGGER.info("Se cumplen ambas condiciones");
			} else {
				System.out.println("No se cumple la condici�n");
				LOGGER.info("No se cumplen ambas condiciones");
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;

	}
}
